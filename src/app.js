'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors')
const conf = require('../config');
const helmet = require('helmet');

const app = express();
const router = express.Router();
const morgan = require('morgan')

var corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200
}

mongoose.connect(conf.connectioString, { useNewUrlParser: true })

const Fundos = require('../models/fundos');
const Fii = require('../models/fiis');
const Contest = require('../models/contest');
const Opcoes = require('../models/opcoes');
const Customer = require('../models/customer');
const Wallet = require('../models/wallet');
const Item = require('../models/item');
const Proventos = require('../models/proventos');
const Historic = require('../models/historic');
const Config = require("../models/configs");
const Dado = require('../models/dado');
const Acoes = require('./acoes/model-acoes');


const customerRoute = require('../routes/customer-route');
const index = require('../routes/index');
const opcoes = require('../routes/opcoes');
const fundos = require('../routes/fundos');
const fii = require('../routes/fii');
const contest = require('../routes/contest');
const wallet = require('../routes/wallet');
const item = require('../routes/item');
const proventos = require('../routes/proventos');
const master = require('../routes/master');
const admin = require('../routes/admin');
const dado = require('../routes/dado');
const acoes = require('./acoes/acoes');

app.use(cors(corsOptions))
// app.use(helmet());

app.use(bodyParser.json({
  limit: '50mb', extended: true
}));
app.use(bodyParser.urlencoded({
  limit: '50mb', extended: true
}));
app.use(morgan('combined'))

app.use('/', index);
app.use('/customers', customerRoute);
app.use('/wallet', wallet);
app.use('/item', item);
app.use('/opcoes', opcoes);
app.use('/fundos', fundos);
app.use('/fii', fii);
app.use('/contest', contest);
app.use('/proventos', proventos);
app.use('/master', master);
app.use("/usuario-admin", admin);
app.use("/dados", dado);
app.use("/acoes", acoes);

module.exports = app;
