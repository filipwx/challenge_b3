'use strict'

const express = require('express');
const router = express.Router();
const controler = require('./controler-acoes.js');

router.get('/', controler.get);
router.get('/delete/:id', controler.deleteAcao);
router.get('/busca/:text', controler.findAcao);

module.exports = router;
