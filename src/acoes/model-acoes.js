'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  ativo: {
    type: String
  },
  valor: {
    type: Number
  },
  pl: {
    type: Number
  },
  nome: {
    type: String
  },
  setor: {
    type: String
  },
  min_52: {
    type: Number
  },
  max_52: {
    type: Number
  },
  mes: {
    type: Number
  },
  lpa: {
    type: Number
  },
  dia: {
    type: Number
  },
  ano_passado: {
    type: Number
  },
  ano_atual: {
    type: Number
  },
});

module.exports = mongoose.model('Acoes', schema)
