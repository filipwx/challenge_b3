'use strict'

const mongoose = require('mongoose');
const Acoes = mongoose.model('Acoes');

exports.get = async (x, y) => {
  if (y == 1) {
    const dados = await Acoes.find({}).sort({ ativo: 'asc'}).limit(x);
    const itens = await Acoes.find({}).estimatedDocumentCount();
    return { dados, itens }
  }
  let skiper = x * (y - 1)
  const dados = await Acoes.find({}).sort({ ativo: 'asc' }).skip(skiper).limit(x);
  const itens = await Acoes.find({}).estimatedDocumentCount();
  return { dados, itens }
}
exports.deleteAcao = async (id) => {
  console.log("Chegou o ID: ", id)
  const acao = await Acoes.findByIdAndRemove(id);
  return acao
}

exports.findAcao = async (termo) => {
  console.log("Chegou o termo: ", termo)
  const acao = await Acoes.aggregate([
    {
      $match: {
        ativo: {
          $regex: termo,
          $options: 'i'
        }
      }
    },
  ]);
  return acao
}
