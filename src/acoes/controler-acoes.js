'use strict'

const repository = require('./repository-acoes.js');

exports.get = (req, res, next) => {
  let limit = Number(req.query.limit) || 500
  let page = Number(req.query.page) || 1
  repository.get(limit, page)
    .then(x => {
      res.status(200).send({
        page: page,
        itens: limit,
        total: x.itens,
        data: x.dados,
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.findAcao = (req, res, next) => {
  repository.findAcao(req.params.text)
    .then(x => {
      res.status(200).send({data: x});
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.deleteAcao = (req, res, next) => {
  repository.deleteAcao(req.params.id)
    .then(x => {
      res.status(200).send({
        message: 'Ação removida com sucesso!',
        return: x
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha ao remover a Ação!'
      });
    })
};
