'use strict'

const mongoose = require('mongoose');
const Fundos = mongoose.model('Fundos');

exports.get = () => {
  return Fundos.find({}, 'fundo_invest_qualif cotistas admin_nome fundo_benchmark patrimonio gestor_nome fundo_tribut_lp gestor_cnpj cvm_classe admin_cnpj fundo_nome_abreviado fundo_cnpj unique_slug fundo_exclusivo situacao fundo_nome fundo_de_cotas fundo_cond_aberto');
}
exports.getById = (id) => {
  return Fundos.findById(id)
}
exports.create = (data) => {
  console.log("Chegou aqui");
  var fundo = new Fundos(data);
  return fundo.save()
}