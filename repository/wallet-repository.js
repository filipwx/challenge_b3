'use strict'
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Wallet = mongoose.model('Wallet');
const Item = mongoose.model('Item');
const Fiis = mongoose.model('Fiis');
const historic = mongoose.model('historic');


exports.get = () => {
  return Wallet.find({});
}
exports.getChartWallet = (x) => {
  return historic.find({ wallet_id: x.wallet_id, user_id: x.user_id });
}
exports.getSingleWallet = (x) => {
  console.log('iten: ', x)
  const itemNome = Item.collection.name
  const fiisNome = Fiis.collection.name
  return Wallet.aggregate([
    {
      $match: { customer: ObjectId(x[0]), cod: x[1] }
    },
    {
      $unwind: {
        path: "$ativos"
      }
    },
    {
      $lookup: {
        from: fiisNome,
        localField: "ativos.ativo",
        foreignField: "_id",
        as: "ativo",
      }
    },
    {
      $project: {
        "ativos.ativo.query": 0,
        "ativos.ativo.comunicados": 0,
        "ativos.ativo.div_list": 0,
        "ativo.query": 0,
        "ativo.comunicados": 0,
        "ativo.div_list": 0,
        "customer": 0,
        "total": 0
      }
    },
    {
      $addFields: {
        valor_mercado: {
          $sum: {
            $multiply: ["$ativos.quantity", { $arrayElemAt: ["$ativo.preco", 0] }]
          }
        },
      }
    },
    {
      $group : {
        _id: {
          codigo: "$cod",
          uuid: "$_id",
          ativo_id: '$ativos.ativo',
          titulo: "$title",
          description: "$description",
          ativo: { $arrayElemAt: ["$ativo.ativo", 0] },
          valor_unit: "$ativos.price",
          quant: "$ativos.quantity",
          preco: { $arrayElemAt: ["$ativo.preco", 0] },
          criacao: "$createdAt",
          total: "$total",
          valor_atual: '$valor_mercado',
          valor_pago: '$ativos.final_total',
        }
      }
    },
    {
      $group : {
        _id: "$_id.ativo",
       dados: { $mergeObjects: "$_id" },
       valor_pag: { $sum: "$_id.valor_pago" },
       valor_now: { $sum: "$_id.valor_atual" },
       qtd_total: { $sum: "$_id.quant" }
      }
    }
  ])
}
exports.getWallets = (x) => {

  const itemNome = Item.collection.name
  const fiisNome = Fiis.collection.name
  return Wallet.aggregate([
    {
      $match: { customer: ObjectId(x), delete: false, enabled: true }
    },
    {$lookup:{
      from: itemNome,
      localField: "_id",
      foreignField: "wallet",
      as: "items_list"
    }},
    {
      $unwind: {
        path: "$items_list",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: fiisNome,
        localField: "items_list.ativo",
        foreignField: "_id",
        as: "items_list.ativo",
      }
    },
    {
      $project: {
        "items_list.ativo.query": 0,
        "items_list.ativo.comunicados": 0,
        "items_list.ativo.div_list": 0,
      }
    },
    { $addFields: {
        paid: {
          $sum: {
            $multiply: ["$items_list.quantity", "$items_list.price"]
          }
        }
      }
    },
    { $group : {
        _id: {
          codigo: "$cod",
          uuid: "$_id",
          titulo: "$title",
          description: "$description",
          enabled: "$enabled",
          investmentProof: "$investmentProof",
          ativo: { $arrayElemAt: ["$items_list.ativo.ativo", 0] },
          preco: { $arrayElemAt: ["$items_list.ativo.preco", 0] },
          criacao: "$createdAt",
          total: "$total",
          ativos: "$ativos",
          dy: { $arrayElemAt: ["$items_list.ativo.dym_6m", 0] },
          num_ativos: { $sum: 1 },
          valor_atual: { $sum: "$paid" },
          valor_pago: { $sum: { $multiply: ["$items_list.quantity", { $arrayElemAt: ["$items_list.ativo.preco", 0] }] } }
        }
      }
    },
    { $group : {
        _id: "$_id.codigo",
        dados: { $mergeObjects: "$_id" },
        itemsSold: { $push: "$_id.ativo" },
        dy_medio: { $avg: "$_id.dy" },
        valor_pag: { $sum: "$_id.valor_pago" },
        valor_now: { $sum: "$_id.valor_atual" }
      }
    }
  ])
}
exports.getAllWallets = (x) => {

  const itemNome = Item.collection.name
  const fiisNome = Fiis.collection.name
  return Wallet.aggregate([
    {
      $match: { customer: ObjectId(x) }
    },
    {$lookup:{
      from: itemNome,
      localField: "_id",
      foreignField: "wallet",
      as: "items_list"
    }},
    {
      $unwind: {
        path: "$items_list",
        preserveNullAndEmptyArrays: true
      }
    },
    {
      $lookup: {
        from: fiisNome,
        localField: "items_list.ativo",
        foreignField: "_id",
        as: "items_list.ativo",
      }
    },
    {
      $project: {
        "items_list.ativo.query": 0,
        "items_list.ativo.comunicados": 0,
        "items_list.ativo.div_list": 0
      }
    },
    { $addFields: {
        paid: {
          $sum: {
            $multiply: ["$items_list.quantity", "$items_list.price"]
          }
        }
      }
    },
    { $group : {
        _id: {
          codigo: "$cod",
          status_disable: "$delete",
          uuid: "$_id",
          titulo: "$title",
          description: "$description",
          ativo: { $arrayElemAt: ["$items_list.ativo.ativo", 0] },
          preco: { $arrayElemAt: ["$items_list.ativo.preco", 0] },
          criacao: "$createdAt",
          dy: { $arrayElemAt: ["$items_list.ativo.dym_6m", 0] },
          num_ativos: { $sum: 1 },
          valor_atual: { $sum: "$paid" },
          valor_pago: { $sum: { $multiply: ["$items_list.quantity", { $arrayElemAt: ["$items_list.ativo.preco", 0] }] } }
        }
      }
    },
    { $group : {
        _id: "$_id.codigo",
        dados: { $mergeObjects: "$_id" },
        itemsSold: { $push: "$_id.ativo" },
        dy_medio: { $avg: "$_id.dy" },
        valor_pag: { $sum: "$_id.valor_pago" },
        valor_now: { $sum: "$_id.valor_atual" }
      }
    }
  ])
}
exports.getById = (id) => {
  return Wallet.findById(id);
}
exports.getSituationWallet = async (id) => {
  let wallet = await Wallet.findOne({ cod: id }).populate('historico.ativo_id', 'ativo')
  return wallet
}
exports.listAllWallets = async () => {
  let wallet = await Wallet.find({delete:false}, '-final_total -agreeHash -customer -total -delete').populate('ativos.ativo', 'valor segmento nome data ativo _id dy')
  return wallet
}
exports.sendWalleter = async (id) => {
  let wallet = await Wallet.findOne({ customer: id, delete: false }).populate('ativos.ativo')
  return wallet
}
exports.createItem = async (data) => {
  var item = new Item(data);
  await item.save()
  return item
}

exports.getExistFii = async (id) => {
  const query = { ativo: ObjectId(id) }
  return await Item.findOne(query);
}

exports.setSituationWallet = async (id, data) => {
  const query = { cod: id }
  const wallet = await Wallet.findOneAndUpdate(query, {
    $push: { historico: data }
  }, { new: true });
  return wallet
}

exports.editItensWallet = async (data) => {
  let wall = data.wallet;
  let ativo = data.ativo;
  delete data.wallet
  let walle = await Wallet.findById(wall);
  let filtrado = walle.ativos.filter( ( elem, index, arr ) => elem.ativo == ativo );
  let itemInserido = filtrado[0] || null
  if (itemInserido) {
    let quantidade = itemInserido.quantity + data.quantity;
    let total1 = itemInserido.final_total + data.final_total;
    let pm = total1 / quantidade
    let subTotal = walle.total - data.final_total;
    itemInserido.quantity = quantidade,
    itemInserido.price = pm,
    itemInserido.final_total = total1
    const wallet = await Wallet.findOneAndUpdate({_id: wall, 'ativos.ativo': ativo }, {
      $set: { 'ativos.$': itemInserido, total: subTotal }
    }, { new: true });
    return wallet;
  }
  let subTotal = walle.total - data.final_total;
  const wallet = await Wallet.findByIdAndUpdate(wall, {
    $push: { ativos: data },
    $set: { total: subTotal }
  }, { new: true });
  return wallet;
}

exports.editTotals = async (data) => {
  let wallet = await Wallet.findById(data.wallet);
  let resultado = wallet.total - data.final_total
  const retorno = await Wallet.findByIdAndUpdate(data.wallet, {total: resultado}, { new: true });
  return retorno;
}

exports.setTotals = async (id, data) => {
  const query = { cod: id }
  const wallet = await Wallet.findOneAndUpdate(query, {
    $inc: { total: data.lucro_liquido, data: 1 }
  }, { new: true });
  return wallet
}

exports.closeItem = async (id, data) => {
  const item = await Item.findByIdAndRemove(id);
  return item
}

exports.updateItem = async (id, data) => {
  const item = await Item.findByIdAndUpdate(id, {
    $set: { quantity: data.total_cotas, price: data.medio }
  });
  return item
}

exports.activateWallet = async (id) => {
  const item = await Wallet.findByIdAndUpdate(id, {
    $set: { enabled: true, enabledDate: new Date(), locked: true }
  }, { new: true });
  return item
}

exports.create = async (data) => {
  var wallet = new Wallet(data);
  await wallet.save()
  return wallet
}

exports.deleteWallet = async (id) => {
  const wallet = await Wallet
    .findByIdAndUpdate(id, {
      $set: {
        delete: true
      }
    });
  return wallet
}
