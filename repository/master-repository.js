'use strict'
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Wallet = mongoose.model('Wallet');
const Item = mongoose.model('Item');
const Fiis = mongoose.model('Fiis');

exports.getAll = () => {
    const itemNome = Item.collection.name
    const fiisNome = Fiis.collection.name
    return Wallet.aggregate([
        {
            $match: { delete: false }
        },
        {
            $lookup: {
                from: itemNome,
                localField: "_id",
                foreignField: "wallet",
                as: "items_list"
            }
        },
        {
            $unwind: {
                path: "$items_list",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: fiisNome,
                localField: "items_list.ativo",
                foreignField: "_id",
                as: "items_list.ativo",
            }
        },
        {
            $project: {
                "items_list.ativo.query": 0,
                "items_list.ativo.comunicados": 0,
                "items_list.ativo.div_list": 0
            }
        },
        {
            $addFields: {
                custo_total: {
                    $sum: {
                        $multiply: ["$items_list.quantity", "$items_list.price"]
                    }
                },
                valor_total: {
                    $sum: {
                        $multiply: ["$items_list.quantity", { $arrayElemAt: ["$items_list.ativo.preco", 0] }]
                    }
                },
                dy: { $arrayElemAt: ["$items_list.ativo.div", 0] }
            }
        },
        {
            $group: {
                _id: {
                    codigo: "$cod",
                    uuid: "$_id",
                    customer: "$customer",
                    titulo: "$title",
                    description: "$description",
                    ativo: { $arrayElemAt: ["$items_list.ativo.ativo", 0] },
                    valor_unit: "$items_list.price",
                    quant: "$items_list.quantity",
                    preco: { $arrayElemAt: ["$items_list.ativo.preco", 0] },
                    criacao: "$createdAt",
                    dividendo: { $arrayElemAt: ["$items_list.ativo.div", 0] },
                    valor_atual: { $sum: "$valor_total" },
                    valor_pago: { $sum: "$custo_total" },
                    posicao_fechada: {
                        $sum: "$total"
                    },
                    resultado: { $subtract: ["$valor_total", "$custo_total"] }
                }
            }
        },
        {
            $group: {
                _id: "$_id.ativo",
                dados: { $mergeObjects: "$_id" },
                itemsSold: { $sum: 1 },
                valor_pag: { $sum: "$_id.valor_pago" },
                valor_now: { $sum: "$_id.valor_atual" },
                qtd_total: { $sum: "$_id.quant" },
                resultado: { $sum: "$_id.resultado" },
                itens_total: { $sum: 1 }
            }
        },
        {
            $addFields: {
                dy_calc: { $divide: ["$dados.dividendo", "$dados.preco"] },
                resultado_atual: { $sum: "$dados.posicao_fechada" }
            }
        },
        {
            $group:{
                _id: "$dados.uuid",
                total_pago: { $sum: "$valor_pag" },
                total: { $sum: "$valor_now" },
                profit_reais: { $sum: "$resultado" },
                wallet_profit: { $last: "$resultado_atual" },
                ativos: { $sum: 1 },
                dados: { $push: "$dados" }
            }
        },
        {
            $addFields: {
                wallet_id: { $arrayElemAt: ["$dados.uuid", 0] },
                user_id: { $arrayElemAt: ["$dados.customer", 0] },
                dy_medio: { "$multiply": [100, { $cond: [{ $gte: ["$profit_reais", 1] }, { $divide: ["$profit_reais", "$total_pago"] }, 0] }] }
            }
        },
        {
            $project: {
                "dados": 0
            }
        },
    ]);
}
exports.getWalletRank = () => {
    const itemNome = Item.collection.name
    const fiisNome = Fiis.collection.name
    return Wallet.aggregate([
        {
            $match: { delete: false, contest: true }
        },
        {
            $lookup: {
                from: itemNome,
                localField: "_id",
                foreignField: "wallet",
                as: "items_list"
            }
        },
        {
            $unwind: {
                path: "$items_list",
                preserveNullAndEmptyArrays: false
            }
        },
        {
            $lookup: {
                from: fiisNome,
                localField: "items_list.ativo",
                foreignField: "_id",
                as: "items_list.ativo",
            }
        },
        {
            $project: {
                "items_list.ativo.query": 0,
                "items_list.ativo.comunicados": 0,
                "items_list.ativo.div_list": 0,
                "total": 0
            }
        },
        {
            $addFields: {
                custo_total: {
                    $sum: {
                        $multiply: ["$items_list.quantity", "$items_list.price"]
                    }
                },
                valor_total: {
                    $sum: {
                        $multiply: ["$items_list.quantity", { $arrayElemAt: ["$items_list.ativo.preco", 0] }]
                    }
                },
                items: { $sum: 1 },
                dy: { $arrayElemAt: ["$items_list.ativo.div", 0] }
            }
        },
        {
            $group: {
                _id: {
                    name: "$title",
                    cod: "$cod"
                },
                itens: { $sum: "$items" },
                valor_atual: { $sum: "$valor_total" },
                valor_pago: { $sum: "$custo_total" }
            }
        },
        {
            $addFields: {
                resultado: { $subtract: ["$valor_atual", "$valor_pago"] }
            }
        },
        {
            $addFields: {
                dy_medio: { "$multiply": [100, { $cond: [{ $gte: ["$resultado", 1] }, { $divide: ["$resultado", "$valor_pago"] }, 0] }] }
            }
        },
        {
            $sort: {
                dy_medio: -1
            }
        },
        { $limit: 10 }
    ]);
}
exports.walletsItens = () => {
    const itemNome = Item.collection.name
    const fiisNome = Fiis.collection.name
    return Wallet.aggregate([
        {
            $match: { delete: false }
        },
        {
            $lookup: {
                from: itemNome,
                localField: "_id",
                foreignField: "wallet",
                as: "items_list"
            }
        },
        {
            $unwind: {
                path: "$items_list",
                preserveNullAndEmptyArrays: false
            }
        },
        {
            $lookup: {
                from: fiisNome,
                localField: "items_list.ativo",
                foreignField: "_id",
                as: "items_list.ativo",
            }
        },
        {
            $project: {
                "items_list.ativo.query": 0,
                "items_list.ativo.comunicados": 0,
                "items_list.ativo.div_list": 0,
                "historico": 0,
                "total": 0
            }
        },
        {
            $group: {
                _id: "$cod",
                ativos: { $sum: 1 },
                dados: { $push: "$items_list.ativo" }
            }
        },
        {
            $project: {
                "dados.adm": 0,
                "dados.cnpj": 0,
                "dados.preco": 0,
                "dados.variacao": 0,
                "dados.close": 0,
                "dados.descricao": 0,
                "dados.sub_name": 0,
                "dados._id": 0,
                "dados.nome": 0
            }
        }
    ]);
}
exports.getSituationWallet = async (id) => {
    let wallet = await Wallet.findOne({ cod: id }).populate('historico.ativo_id', 'ativo dividendos_list')
    return wallet
}