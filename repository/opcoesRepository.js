'use strict'

const mongoose = require('mongoose');
const Opcoes = mongoose.model('Opcoes');

exports.getById = (id) => {
    return Opcoes.findById(id)
}
exports.get = () => {
    return Opcoes.find({});
}
