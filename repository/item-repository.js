'use strict'
const mongoose = require('mongoose');
const Item = mongoose.model('Item');
const Wallet = mongoose.model('Wallet');
const Fiis = mongoose.model('Fiis');
const ObjectId = mongoose.Types.ObjectId;


exports.create = async (data) => {
  var item = new Item(data);
  await item.save()
  return item
}

exports.getSell = async (data) => {
  console.log(data);
  const fiisNome = Fiis.collection.name
  return await Item.aggregate([
    {
      "$match": {
        "wallet": ObjectId(data.wallet),
        "ativo": ObjectId(data.ativo)
      }
    },
    {
      "$lookup": {
        "from": fiisNome,
        "localField": "ativo",
        "foreignField": "_id",
        "as": "ativo_list"
      }
    },
    {
      "$project": {
        "ativo_list.query": 0,
        "ativo_list.comunicados": 0,
        "ativo_list.div_list": 0,
        "ativo_list.descricao": 0,
        "customer": 0,
        "total": 0
      }
    },
    {
      "$addFields": {
        "custo_total": {
          "$sum": {
            "$multiply": [
              "$quantity",
              "$price"
            ]
          }
        },
        "fundo": {
          "$arrayElemAt": [
            "$ativo_list.ativo",
            0.0
          ]
        }
      }
    },
    {
      "$group": {
        "_id": "$fundo",
        "valor_pago": {
          "$sum": "$custo_total"
        },
        "qtd": {
          "$sum": "$quantity"
        }
      }
    }
  ])
}
