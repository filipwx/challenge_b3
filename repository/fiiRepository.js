'use strict'

const mongoose = require('mongoose');
const Fii = mongoose.model('Fiis');

exports.getById = (id) => {
  console.log(id);
  return Fii.findOne({ ativo: id })
}
exports.get = () => {
  return Fii.find({}, '-query -comunicados -div_list -ativos_fundo -ativos_cidade');
}
exports.getAll = () => {
  return Fii.find({});
}
exports.create = (data) => {
  var fii = new Fii(data);
  return fii.save()
}
exports.editFii = async (id, data) => {
  let query = { ativo: id };
  const item = await Fii.findOneAndUpdate(query, {
    $set: data
  }, {new: true});
  return item
}
exports.closeFii = async (id) => {
  const item = await Fii.findByIdAndRemove(id);
  return item
}
