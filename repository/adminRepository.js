"use strict";
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const Wallet = mongoose.model("Wallet");
const historic = mongoose.model("historic");
const Customer = mongoose.model("Customer");
const Contest = mongoose.model("Contest");
const Config = mongoose.model("Config");


exports.getWallets = () => {
  return Wallet.find({});
};

exports.getUsers = () => {
  return Customer.find({});
};
exports.getContest = () => {
  return Contest.find({});
};






exports.getConfig = () => {
  return Config.find({});
};
exports.setConfig = async data => {
  var config = new Config(data);
  await config.save();
  return config;
};
exports.editConfigs = async (id, data) => {
  const res = await Config.findByIdAndUpdate(id, {
    $set: { login: data.login, cadastro: data.cadastro, toolbar: data.toolbar }
  });
  return res;
};