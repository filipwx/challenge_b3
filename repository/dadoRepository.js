'use strict'

const mongoose = require('mongoose');
const Dado = mongoose.model('Dado');

exports.getIbov = async  () => {
  return await Dado.aggregate([
    {
      $match: { index_ticker: "IBOV" }
    },
    {
      $unwind: {
        path: "$index_quotes"
      }
    },
    {
      $sort: {
        "index_quotes.d": -1
      }
    },
    { $limit: 40 },
    {
      $sort: {
        "index_quotes.d": 1
      }
    },
    {
      $group: {
        _id: "$_id",
        index_quotes: {
          $push: "$index_quotes"
        }
      }
    },
    {
      $unwind: {
        path: "$index_quotes"
      }
    },
    {
      $sort: {
        "index_quotes.d": 1
      }
    },
    {
      $group: {
        _id: "$_id",
        index_quotes: {
          $push: "$index_quotes"
        }
      }
    }
  ]);
};
exports.getCdi = async  () => {
  return await Dado.aggregate([
    {
      $match: { index_ticker: "CDI" }
    },
    {
      $unwind: {
        path: "$index_quotes"
      }
    },
    {
      $sort: {
        "index_quotes.d": -1
      }
    },
    { $limit: 40 },
    {
      $sort: {
        "index_quotes.d": 1
      }
    },
    {
      $group: {
        _id: "$_id",
        index_quotes: {
          $push: "$index_quotes"
        }
      }
    },
    {
      $unwind: {
        path: "$index_quotes"
      }
    },
    {
      $sort: {
        "index_quotes.d": 1
      }
    },
    {
      $group: {
        _id: "$_id",
        index_quotes: {
          $push: "$index_quotes"
        }
      }
    }
  ]);
};
