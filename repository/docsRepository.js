'use strict'

const mongoose = require('mongoose');
const Docs = mongoose.model('Docs');

exports.get = () => {
  return Docs.find({}, 'data link titulo fiis').populate('fiis', 'ativo')
}
