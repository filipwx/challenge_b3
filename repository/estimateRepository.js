'use strict'
const mongoose = require('mongoose');
const Estimate = mongoose.model('Estimate');


exports.get = () => {
  return Estimate.find({});
}
exports.getSelf = (userId) => {
  return Estimate.find({
    costumer: userId
  });
}
exports.create = async (data) => {
    var estimate = new Estimate(data);
    await estimate.save()
    return estimate
}

exports.update = async (id, data) => {
  const estimate = await Estimate
    .findByIdAndUpdate(id, {
      $set: {
        status: data.status
      }
    });
  return estimate
}
