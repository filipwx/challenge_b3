'use strict'

const mongoose = require('mongoose');
const Contest = mongoose.model('Contest');

exports.getById = (id) => {
  return Contest.findById(id)
}
exports.get = () => {
  return Contest.find({});
}

exports.create = async (data) => {
  var contest = new Contest(data);
  await contest.save();
  return contest
}
exports.activateWallet = async (id, data) => {
  let payload = { wallet: data.wallet, customer: data.customer }
  const res = await Contest.findByIdAndUpdate(id, {
    $push: { wallets: payload  }
  });
  return res
}
exports.update = async (id, data) => {
  const res = await Contest.findByIdAndUpdate(id, {
    $set: {
      nome: data.nome,
      cod: data.cod,
      description: data.description,
      type: data.type,
      price: data.price,
      state: data.state,
      startDate: data.startDate,
      endDate: data.endDate
    }
  });
  return res;
};
