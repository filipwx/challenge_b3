'use strict'

const mongoose = require('mongoose');
const Loser = mongoose.model('Loser');

exports.get = () => {
  return Loser.find({})
}
