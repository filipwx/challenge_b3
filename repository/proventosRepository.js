'use strict'

const mongoose = require('mongoose');
const Proventos = mongoose.model('Proventos');

exports.get = () => {
  return Proventos.find({}, 'data valor fundo fiis').populate('fiis', 'ativo')
}
