'use strict'

const mongoose = require('mongoose');
const Customer = mongoose.model('Customer');
const ValidationContract = require('../validator/validator');
const repository = require('../repository/custumer-repository');
const md5 = require('md5');
const authService = require('../services/auth');
const mailer = require('../services/mailer');
const crypto = require('crypto');

exports.post = async (req, res, next) => {

  let contract = new ValidationContract();

  contract.hasMinLen(req.body.fullname, 10, 'Seu nome deve conter mais de 10 caracteres não é?');
  contract.hasMinLen(req.body.nick, 4, 'Seu apelido deve conter mais de 4 caracteres!');
  contract.isEmail(req.body.email, 'Esse email está correto?');
  contract.hasMinLen(req.body.password, 6, 'A Senha deve conter mais de 6 caracteres!');

  if (!contract.isValid()) {
    res.status(400).send(contract.errors()).end();
    return
  }

  try {
    const costumer = await repository.create({
      fullname: req.body.fullname,
      nick: req.body.nick,
      email: req.body.email,
      state: req.body.state,
      password: md5(req.body.password + global.SALT_KEY)
    })
    const token = crypto.randomBytes(20).toString("hex");
    const user = costumer.nick

    await Customer.findByIdAndUpdate(costumer._id, {
      $set: { accountTokenValidate: token }
    });

    mailer.sendMail({
      to: req.body.email,
      from: "Carteira Soberana contact@oxigen.club",
      replyTo: "contact@oxigen.club",
      subject: `Bem vindo a Carteira Soberana ${req.body.nick}!`,
      template: "welcome/welcome",
      context: { token, user }
    });

    res.status(201).send({
      message: 'Usuario cadastrado com sucesso!',
      data: costumer
    });

  } catch (e) {
    console.log(e);
    res.status(400).send({
      message: 'Falha ao cadastrar',
      erro: e
    });
  }

};
exports.put = async (req, res, next) => {
  try {
    const costumer = await repository.update(req.headers['user-id'], req.body);
    res.status(200).send({
      message: 'Atualizado com sucesso!',
      data: costumer
    });
  } catch (e) {
    res.status(500).send({
      message: 'Falha ao processar sua requisiÃ§Ã£o'
    });
  }
};
exports.fogot = async (req, res, next) => {
  try {
    const costumer = await repository.forgot(req.body.email);

    const token = crypto.randomBytes(20).toString('hex');
    const user = costumer.nick
    const now = new Date();
    now.setHours(now.getHours() + 1)

    await Customer.findByIdAndUpdate(costumer._id, {
      $set: {
        passResetToken: token,
        passResetExpires: now
      }
    });
    mailer.sendMail({
      to: req.body.email,
      from: 'Oxigen Club contact@oxigen.club',
      replyTo: 'contact@oxigen.club',
      subject: 'Email de recuperação de senha',
      template: 'forget/forgot',
      context: { token, user }
    })
    res.status(200).send({message: 'Email de redefinição de senha enviado com sucesso, ou não ;-) !'});

  } catch (e) {
    res.status(500).send(e);
  }
};
exports.verificar = async (req, res, next) => {

  try {
    if (req.body.token === 'xxxxxxxx') {
      res.status(200).send({ message: 'Email confirmado com sucesso!' });
      return 0;
    }
    const costumer = await repository.verificar(req.body.token);

    var query = { email: costumer.email };

    await Customer.findOneAndUpdate(query, {
      $set: { status: 'active', accountTokenValidate: null }
    });

    res.status(200).send({ message: 'Email confirmado com sucesso!' });

  } catch (e) {
    res.status(400).send({message: 'Erro ao validar o email!', error: e});
  }
};
exports.redefinir = async (req, res, next) => {

  try {
    const costumer = await repository.redefinir(req.body.token);
    const now = new Date();
    if (now > costumer.passResetExpires) {
      res.status(400).send({error: "Opss! Esse token já expirou"});
      return;
    }

    var query = { email: req.body.email };

    await Customer.findOneAndUpdate(query, {
      $set: {
        password: md5(req.body.password + global.SALT_KEY),
        passResetToken: null,
        passResetExpires: new Date()
      }
    });

    res.status(200).send({ message: 'Senha redefinida com sucesso!' });

  } catch (e) {
    res.status(500).send(e);
  }
};
exports.trocarSenha = async (req, res, next) => {

  try {
    const actual = md5(req.body.atual + global.SALT_KEY)
    const costumer = await repository.trocarSenha(actual, req.body.email);
    const costumer2 = await repository.getById(req.headers['user-id']);

    if (!costumer) {
      res.status(400).send({error: "Opss! Erro ao trocar a senha!"});
      return;
    }
    if (!costumer2) {
      res.status(400).send({error: "Opss! Erro ao trocar a senha!"});
      return;
    }

    if (String(costumer._id) === String(costumer2._id)) {
      var query = { _id: req.headers['user-id'] };
      await Customer.findOneAndUpdate(query, {
        $set: {
          password: md5(req.body.password + global.SALT_KEY),
          passResetToken: null,
          passResetExpires: null
        }
      });
      res.status(200).send({ message: 'Senha redefinida com sucesso!' });
    }

  } catch (e) {
    res.status(500).send(e);
  }
};
exports.getProfile = async (req, res, next) => {
  try {
    const costumer = await repository.getById(req.headers['user-id']);
    res.status(200).send({
      message: 'Perfil encontrado',
      data: costumer
    });
  } catch (e) {
    res.status(500).send({
      message: 'Falha ao processar sua requisiÃ§Ã£o'
    });
  }
};

exports.authenticate = async (req, res, next) => {
  try {
    const customer = await repository.authenticate({
      email: req.body.email,
      password: md5(req.body.password + global.SALT_KEY)
    });
    if (!customer) {
      res.status(404).send({
        message: 'UsuÃ¡rio ou senha invÃ¡lidos'
      });
      return;
    }
    const token = await authService.generateToken({
      id: customer._id,
      email: customer.email,
      name: customer.name
    });
    res.status(201).send({
      token: token,
      data: customer
    });
    var ip = req.headers['x-forwarded-for'] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      (req.connection.socket ? req.connection.socket.remoteAddress : null);
      console.log(ip);

  } catch (e) {
    res.status(500).send({
      message: 'Falha ao processar sua requisiÃ§Ã£o'
    });
  }
};

exports.refreshToken = async (req, res, next) => {
  try {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    const data = await authService.decodeToken(token);

    const customer = await repository.getById(data.id);

    if (!customer) {
      res.status(404).send({
        message: 'Cliente nÃ£o encontrado'
      });
      return;
    }

    const tokenData = await authService.generateToken({
      id: customer._id,
      email: customer.email,
      name: customer.name
    });

    res.status(201).send({
      token: tokenData,
      data: {
        email: customer.email,
        name: customer.name
      }
    });
  } catch (e) {
    res.status(500).send({
      message: 'Falha ao processar sua requisiÃ§Ã£o'
    });
  }
};
