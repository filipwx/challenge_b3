'use strict'

const repository = require('../repository/dadoRepository');

exports.getIbov = (req, res, next) => {
  console.log("getIbov");
  repository.getIbov()
    .then(x => {
      res.status(200).send(x);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.getCdi = (req, res, next) => {
  console.log("getCdi");
  repository.getCdi()
    .then(x => {
      res.status(200).send(x);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
