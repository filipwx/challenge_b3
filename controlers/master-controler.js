'use strict'

const repository = require('../repository/master-repository');
const alpha = require('../services/alpha-prices');
const moment = require('moment');
moment.locale('pt-BR');

exports.get = (req, res, next) => {
    console.log("Get All Master");
    repository.getAll()
    .then(data => {
        res.status(200).send(data);
    })
    .catch(e => {
        res.status(400).send({
            message: 'Falha na requisição',
            error: e
        });
    })
};
exports.getwalletsrank = (req, res, next) => {
    console.log("Get Wallets Ranking");
    repository.getWalletRank()
    .then(data => {
        res.status(200).send(data);
    })
    .catch(e => {
        res.status(400).send({
            message: 'Falha na requisição',
            error: e
        });
    })
};
exports.walletsItens = (req, res, next) => {
    console.log("Get Wallets Itens");
    repository.walletsItens()
    .then(data => {
        res.status(200).send(data);
    })
    .catch(e => {
        res.status(400).send({
            message: 'Falha na requisição',
            error: e
        });
    })
};
exports.queryPrice = async (req, res, next) => {
    console.log("Query get price");

    const retorno = await alpha.queryPrice(req.body);
    res.status(200).send({ data: retorno });
};
exports.getSituationWallet = (req, res, next) => {
    console.log(`Master Get Situation Wallet: ${req.query.cod}`);
    repository.getSituationWallet(req.query.cod)
    .then(data => {
        const historico = data.historico;
        const final_list = []
        historico.forEach(function (value) {
            let lista = value.ativo_id.dividendos_list;
            const data_compra = moment(value.op_date);

            const listaFinal = lista.filter(iten => {
                let data_baser = moment(iten.data_base);
                const differ = data_compra.diff(data_baser, 'days');
                return differ < 0;
            });
            value.ativo_id.dividendos_list = 0;
            const dados = {
                ativo_info: value,
                dividendos_distribuidos: listaFinal
            };
            final_list.push(dados);
        });
        res.status(200).send(final_list);
    })
    .catch(e => {
        res.status(400).send({
            message: 'Falha na requisição',
            error: e
        });
    })
};