'use strict'

const repository = require('../repository/fundosRepository');

exports.getById = (req, res, next) => {
  repository.getById(req.params.tag)
    .then(data => {
      res.status(200).send(data);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.get = (req, res, next) => {
  repository.get()
    .then(x => {
      res.status(200).send({
        data: x
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};

exports.post = (req, res, next) => {
  repository.create(req.body)
    .then(x => {
      res.status(201).send({
        message: 'Perfil atualizado com sucesso!',
        retunr: x
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha'
      });
    })
};
