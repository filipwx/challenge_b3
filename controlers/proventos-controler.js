'use strict'

const repository = require('../repository/proventosRepository');

exports.get = (req, res, next) => {

  repository.get()
    .then(x => {
      res.status(200).send(x);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};