'use strict'

const repository = require('../repository/wallet-repository');
const repositoryContest = require('../repository/contestRepository');
const guid = require('guid');

exports.setNewItemWallet = (req, res, next) => {
  console.log("setNewItemWallet: ", req.body);
  repository.editItensWallet(req.body)
    .then(data => {
      res.status(200).send({
        message: 'Carteira atualizada com sucesso!',
        data: data
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.getById = (req, res, next) => {
  console.log("getById");
  repository.getById(req.params.id)
    .then(data => {
      res.status(200).send(data);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.getSituationWallet = (req, res, next) => {
  console.log("getSituationWallet");
  repository.getSituationWallet(req.query.cod)
    .then(data => {
      res.status(200).send(data.historico);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.getChartWallet = (req, res, next) => {
  console.log("getChartWallet");
  repository.getChartWallet(req.body)
    .then(data => {
      res.status(200).send(data);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};

exports.setSituationWallet = (req, res, next) => {
  console.log("setSituationWallet");
  function workdir_buy(x) {
    let recebido = parseFloat(req.body.qtd_atual * req.body.preco_medio)
    let emcasa = parseFloat(x.quantity * x.price)
    let total = emcasa + recebido
    let cotas = parseInt(req.body.qtd_atual) + parseInt(x.quantity)
    let preco_med = total / cotas
    return { total_cotas: cotas, medio: preco_med }
  }
  function newItem(x) {
    var item = {
      wallet: x.walet_id,
      ativo: x.ativo_id,
      quantity: x.qtd_atual,
      price: x.preco_medio,
      data: x.op_date
    }
    console.log(item);

    repository.createItem(item)
    .then(data => {
      salvaHistorico(data)
      res.status(200).send({
        message: 'Operação salva com sucesso!',
        return: data
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
  }
  function salvaHistorico(x) {
    var item = {
      ativo_id: req.body.ativo_id,
      lucro_pcnt: req.body.lucro_pcnt,
      pcnt_liqd: req.body.pcnt_liqd,
      preco_venda: req.body.preco_venda,
      total_da_venda: req.body.total_da_venda,
      imposto: req.body.imposto,
      lucro_bruto: req.body.lucro_bruto,
      lucro_liquido: req.body.lucro_liquido,
      qtd_atual: req.body.qtd_atual,
      type: req.body.type,
      preco_medio: req.body.preco_medio,
      value_paid: req.body.value_paid,
      op_date: req.body.op_date
    }
    console.log(item);

    repository.setSituationWallet(req.body.wallet_name, item)
    .then(data => {
      res.status(200).send({
        message: 'Operação salva com sucesso!',
        return: data
      });
    })
    .catch(e => {
      console.log(e);
    })
  }
  function salvaVenda(x) {
    var item = {
      ativo_id: req.body.ativo_id,
      lucro_pcnt: req.body.lucro_pcnt,
      pcnt_liqd: req.body.pcnt_liqd,
      preco_venda: req.body.preco_venda,
      total_da_venda: req.body.total_da_venda,
      imposto: req.body.imposto,
      lucro_bruto: req.body.lucro_bruto,
      lucro_liquido: req.body.lucro_liquido,
      qtd_atual: req.body.qtd_atual,
      type: req.body.type,
      preco_medio: req.body.preco_medio,
      value_paid: req.body.value_paid,
      op_date: req.body.op_date
    }
    repository.setSituationWallet(req.query.cod, item)
    .then(data => {
      incrementTotals(req.query.cod, req.body)
      res.status(200).send({
        message: 'Operação salva com sucesso!',
        return: data
      });
    })
    .catch(e => {
      console.log(e);
    })
  }
  function incrementTotals(x) {
    var item = {
      lucro_liquido: req.body.lucro_liquido
    }
    repository.setTotals(req.query.cod, item)
    .then(data => {

    })
    .catch(e => {
      console.log(e);
    })
  }

  function updateItem(id, data) {
    repository.updateItem(id, data)
    .then(data => {
      salvaHistorico(data)
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
  }
  function closingPosition(id, data) {
    repository.closeItem(id, data)
    .then(data => {
      salvaVenda(data)
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
  }


  if (req.body.type == 'buy') {
    var iten_saved = {}
    repository.getExistFii(req.body.ativo_id)
    .then(data => {
      if (data != null) {
        iten_saved.quantity = data.quantity
        iten_saved.price = data.price
        var result = workdir_buy(iten_saved)
        updateItem(data._id, result)
      }else{
        newItem(req.body)
      }
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })

  }else{
    var iten_saved = {}
    repository.getExistFii(req.body.ativo_id)
    .then(data => {
      if (data != null) {
        let itens = {
          final_price: req.body.preco_venda,
          final_total: req.body.total_da_venda,
          close: true,
          close_date: new Date()
        }
        closingPosition(data._id, itens)
      }
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
  }
};
exports.deleteWallet = (req, res, next) => {
  console.log("Delete Wallet");
  repository.deleteWallet(req.params.id)
    .then(data => {
      res.status(200).send(data);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.get = (req, res, next) => {
  console.log("get");
  repository.get()
    .then(x => {
      res.status(200).send(x);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};

exports.listObj = (req, res, next) => {
  console.log("listObj");
  repository.listObj()
    .then(x => {
      res.status(200).send(x);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.listAllWallets = (req, res, next) => {
  console.log("listAllWallets");
  repository.listAllWallets()
    .then(x => {
      res.status(200).send(x);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.getSingleWallet = (req, res, next) => {
  console.log("getSingleWallet: ", req.query.cod);
  repository.getSingleWallet([req.headers['user-id'], req.query.cod])
  .then(x => {
    res.status(200).send(x);
  })
  .catch(e => {
    console.log(e);
    res.status(400).send({
      message: 'Falha'
    });
  })
};
exports.getSelf = (req, res, next) => {
  console.log("getSelf");
  repository.getSelf(req.headers['user-id'])
  .then(x => {
    res.status(200).send(x);
  })
  .catch(e => {
    console.log(e);
    res.status(400).send({
      message: 'Falha'
    });
  })
};
exports.getWallet = (req, res, next) => {
  console.log("getWallet: ", req.headers['user-id']);
  repository.sendWalleter(req.headers['user-id'])
  .then(x => {
    res.status(200).send(x);
  })
  .catch(e => {
    console.log(e);
    res.status(400).send({
      message: 'Falha na requisição das carteiras'
    });
  })
};
exports.getAllWallets = (req, res, next) => {
  console.log("getAllWallets");
  repository.getAllWallets(req.headers['user-id'])
  .then(x => {
    res.status(201).send(x);
  })
  .catch(e => {
    console.log(e);
    res.status(400).send({
      message: 'Falha na requisição das carteiras'
    });
  })
};
exports.post = (req, res, next) => {
  let ip = req.headers['x-forwarded-for'] ||
     req.connection.remoteAddress ||
     req.socket.remoteAddress ||
     (req.connection.socket ? req.connection.socket.remoteAddress : null);
  let userAgent = req.headers['user-agent']
  let data = new Date().toString().replace(/T/, ':').replace(/\.\w*/, '');

  let hashe = ip +'-'+ userAgent +'-'+ data +'-'+ req.headers['user-id']
  let wall = {
    customer: req.body.customer || req.headers['user-id'],
    cod: "WALL-" + guid.raw().substring(0, 6),
    agreeHash: hashe,
    title: req.body.title,
    investmentProof: req.body.investmentProof || '',
    description: req.body.description || ''
  }
  repository.create(wall)
    .then(x => {
      res.status(201).send({
        message: 'Carteira criada com sucesso!',
        return: x
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha',
        erro: e
      });
    })
};
exports.activateWallet = async (req, res, next) => {
  try {
    let payload = { wallet: req.body.wallet, customer: req.body.customer }
    await repositoryContest.activateWallet(req.body.contest, payload)
    const resultado = await repository.activateWallet(req.body.wallet)
    res.status(200).send({
      message: 'Carteira ativada com sucesso!',
      data: resultado
    });
  } catch (e) {
    res.status(400).send({
      message: 'Falha na requisição',
      error: e
    });
  }
};
