'use strict'

const repository = require('../repository/opcoesRepository');

exports.getById = (req, res, next) => {
    repository.getById(req.params.id)
        .then(data => {
            res.status(200).send(data);
        })
        .catch(e => {
            res.status(400).send({
                message: 'Falha na requisição',
                error: e
            });
        })
};
exports.get = (req, res, next) => {
    repository.get()
        .then(x => {
            res.status(200).send({
                data: x
            });
        })
        .catch(e => {
            res.status(400).send({
                message: 'Falha na requisição',
                error: e
            });
        })
};
