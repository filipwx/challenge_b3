'use strict'

const repository = require('../repository/orderRepository');
const guid = require('guid');

exports.post = async(req, res, next) => {

    try {
        const order = await repository.create({
            costumer: req.body.costumer,
            cod: guid.raw().substring(0, 6),
            items: req.body.items
        })
        res.status(201).send({
            message: 'Orçamento registrado com sucesso',
            data: order
        });

    } catch (e) {
        res.status(400).send({
            message: 'Falha na requisição'
        });
    }

};
exports.get = async(req, res, next) => {
  try {
    var data = await repository.get();
    res.status(200).send(data);
  } catch (error) {
    res.status(400).send(error);
  }
};
exports.getById = async(req, res, next) => {
  try {
    var data = await repository.getById(req.params.id);
    res.status(200).send(data);
  } catch (error) {
    res.status(400).send(error);
  }
};
exports.getSelf = async(req, res, next) => {
  try {
    var data = await repository.getSelf(req.headers['user-id']);
    res.status(200).send(data);
  } catch (error) {
    res.status(400).send(error);
  }
};
exports.put = async (req, res, next) => {
  try {
    const data = await repository.update(req.params.id, req.body);
    res.status(200).send({
      message: 'Atualizado com sucesso!'
    });
  } catch (e) {
    res.status(500).send({
      message: 'Falha ao processar sua requisição'
    });
  }
};
