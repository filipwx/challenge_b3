'use strict'

const repository = require('../repository/contestRepository');
const guid = require('guid');

exports.getById = (req, res, next) => {
  repository.getById(req.params.id)
    .then(data => {
      res.status(200).send(data);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.get = (req, res, next) => {
  repository.get()
    .then(x => {
      res.status(200).send({
        data: x
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.post = (req, res, next) => {
  let item = {
    nome: req.body.nome,
    cod: "COD-" + guid.raw().substring(0, 6),
    description: req.body.description,
    type: req.body.type,
    price: req.body.price,
    state: req.body.state,
    startDate: req.body.startDate,
    endDate: req.body.endDate
  }
  repository.create(item)
    .then(x => {
      res.status(200).send({
        message: 'Competição criada com sucesso!',
        data: x
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
