'use strict'

const repository = require('../repository/fiiRepository');

exports.deleteFii = (req, res, next) => {
  repository.closeFii(req.params.id)
    .then(data => {
      res.status(200).send(data);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.getById = (req, res, next) => {
  repository.getById(req.params.id)
    .then(data => {
      res.status(200).send(data);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.get = (req, res, next) => {

  repository.get()
    .then(x => {
      res.status(200).send(x);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.getAll = (req, res, next) => {
  repository.getAll()
    .then(x => {
      res.status(200).send(x);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha na requisição',
        error: e
      });
    })
};
exports.editFii = (req, res, next) => {
  console.log(req.body, req.params.id);
  repository.editFii(req.params.id, req.body)
    .then(x => {
      res.status(201).send({
        message: 'FII atualizado com sucesso!',
        retunr: x
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha ao atualizar o FII'
      });
    })
};
