'use strict'

const repository = require('../repository/item-repository');
const wallets = require('../repository/wallet-repository');

exports.post = async (req, res, next) => {
  await wallets.editTotals(req.body)
  repository.create(req.body)
    .then(x => {
      res.status(201).send({
        message: 'Entrada atualizada com sucesso!',
        return: x
      });
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha'
      });
    })
};

exports.processSell = (req, res, next) => {
  repository.getSell(req.body)
    .then(x => {
      res.status(200).send(x);
    })
    .catch(e => {
      res.status(400).send({
        message: 'Falha'
      });
    })
};
