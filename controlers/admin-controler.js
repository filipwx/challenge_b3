"use strict";

const repository = require("../repository/adminRepository");
const user = require("../repository/custumer-repository");
const repositoryContest = require("../repository/contestRepository");
const authService = require("../services/auth");
const guid = require("guid");

exports.getAllWallets = async  (req, res, next) => {
  console.log("getAllWallets");
  try {
    const retorno = await repository.getWallets();
    res.status(200).send({ data: retorno });
  } catch (error) {
    res.status(400).send({ message: "Falha na requisição", error: error });
  }
};
exports.getAllConfigs = async (req, res, next) => {
  console.log("getAllConfigs");
  try {
    const retorno = await repository.getConfig();
    res.status(200).send({ data: retorno });
  } catch (error) {
    res.status(400).send({ message: "Falha na requisição", error: error });
  }
};
exports.getConfig = async (req, res, next) => {
  console.log("getConfig");
  try {
    const retorno = await repository.getConfig();
    let resumo = {
      toolbar: retorno[0].toolbar,
      cadastro: retorno[0].cadastro,
      login: retorno[0].login
    };
    res.status(200).send({ data: resumo });
  } catch (error) {
    res.status(400).send({ message: "Falha na requisição", error: error });
  }
};

exports.setConfigs = async (req, res, next) => {
  console.log("setConfigs");
  let configuracoes = {
    login: req.body.login,
    cadastro: req.body.cadastro,
    toolbar: req.body.toolbar
  };
  try {
    const retorno = await repository.setConfig(configuracoes);
    res.status(200).send({ data: retorno });
  } catch (error) {
    res.status(400).send({ message: "Falha na requisição", error: error });
  }
};
exports.editConfigs = async (req, res, next) => {
  console.log("editConfigs");
  let configuracoes = {
    login: req.body.login,
    cadastro: req.body.cadastro,
    toolbar: req.body.toolbar
  };
  try {
    const retorno = await repository.editConfigs(req.body.id, configuracoes);
    res.status(200).send({ data: retorno });
  } catch (error) {
    res.status(400).send({ message: "Falha na requisição", error: error });
  }
};

exports.getAllUsers = async  (req, res, next) => {
  console.log("getAllUsers");
  try {
    const retorno = await repository.getUsers();
    res.status(200).send({ data: retorno });
  } catch (error) {
    res.status(400).send({ message: "Falha na requisição", error: error });
  }
};

exports.getAllContest = async (req, res, next) => {
  console.log("getAllContest");
  try {
    const retorno = await repository.getContest();
    res.status(200).send({ data: retorno });
  } catch (error) {
    res.status(400).send({ message: "Falha na requisição", error: error });
  }
};
exports.createContest = async (req, res, next) => {
  console.log("createContest");
  let item = {
    nome: req.body.nome,
    cod: "COD-" + guid.raw().substring(0, 6),
    description: req.body.description,
    type: req.body.type,
    price: req.body.price,
    state: req.body.state,
    startDate: req.body.startDate,
    endDate: req.body.endDate
  };
  try {
    const retorno = await repositoryContest.create(item);
    res.status(200).send({ message: 'Competição criada com sucesso!', data: retorno });
  } catch (error) {
    res.status(400).send({ message: "Falha na requisição", error: error });
  }
};

exports.editContest = async (req, res, next) => {
  console.log("editContest");
  let item = {
    nome: req.body.nome,
    cod: "COD-" + guid.raw().substring(0, 6),
    description: req.body.description,
    type: req.body.type,
    price: req.body.price,
    state: req.body.state,
    startDate: req.body.startDate,
    endDate: req.body.endDate
  };
  try {
    const retorno = await repositoryContest.update(req.params.id, item);
    res.status(200).send({ message: 'Competição criada com sucesso!', data: retorno });
  } catch (error) {
    res.status(400).send({ message: "Falha na requisição", error: error });
  }
};

exports.authenticate = async (req, res, next) => {
  try {
    const customer = await user.authenticateToken({
      token: req.body.token,
    });
    if (!customer) {
      res.status(401).send({
        message: "Token invalido, sintimos muito."
      });
      return;
    }
    const token = await authService.generateToken({
      id: customer._id,
      email: customer.email,
      name: customer.name
    });
    res.status(200).send({
      token: token,
      data: customer
    });
    var ip =
      req.headers["x-forwarded-for"] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      (req.connection.socket ? req.connection.socket.remoteAddress : null);
    console.log(ip);
  } catch (e) {
    res.status(500).send({
      message: "Falha ao processar sua requisiÃ§Ã£o"
    });
  }
};