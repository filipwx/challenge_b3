'use strict'

var config = require('../config');
var cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: 'deolsgbut',
    api_key: config.cloudinaryAPIKey,
    api_secret: config.cloudinaryAPISecret
});

exports.image = async(file) => {
    const resulter = await cloudinary.v2.uploader.upload(file)
    console.log(resulter);
    return resulter;

}
