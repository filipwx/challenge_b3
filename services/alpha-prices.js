const axios = require('axios');
const config = require('../config');
const alpha = require('alphavantage')({ key: config.alphakey });

exports.queryPrice = async (entry) => {
  console.log(entry);
  let value = entry.symbol + '.sa';
  try {
    const response = await alpha.data.intraday(value)
    return response;
  } catch (error) {
    return { error: "Erro na requisição"};
  }
};
