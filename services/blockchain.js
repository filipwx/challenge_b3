'use strict';
const driver = require('bigchaindb-driver');


exports.decodeToken = async (data) => {
  const alice = new driver.Ed25519Keypair()
  const conn = new driver.Connection('https://blockchain.oxigen.club/api/v1/')
  const tx = await driver.Transaction.makeCreateTransaction(
    {
      message: data,
      data: Date(Date.now()),
    },
    null,
    [ driver.Transaction.makeOutput(
      driver.Transaction.makeEd25519Condition(alice.publicKey))],
      alice.publicKey)
      const txSigned = await driver.Transaction.signTransaction(tx, alice.privateKey)
      const teste = await conn.postTransactionCommit(txSigned)
      return teste;
}
