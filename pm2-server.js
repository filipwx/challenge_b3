module.exports = {
  apps : [{
    name        : "Challenge B3",
    script      : "bin.js",
    watch       : true,
    merge_logs  : true,
    cwd         : "/var/www/bin/",
   }]
}
