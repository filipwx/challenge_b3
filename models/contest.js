'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    nome: {
        type: String,
        required: true
    },
    cod: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum: ['paid', 'free'],
        required: true,
        default: 'free'
    },
    price: {
        type: Number,
        required: true,
        default: 0
    },
    state: {
        type: String,
        enum: ['active', 'deactive'],
        required: true,
        default: 'deactive'
    },
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: new Date()
    },
    wallets: [{
      wallet: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Wallet'
      },
      customer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Customer'
      },
      createdAt: {
          type: Date,
          required: true,
          default: new Date()
      }
    }]
});

module.exports = mongoose.model('Contest', schema)
