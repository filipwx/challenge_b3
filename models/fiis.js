'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  nome: {
    type: String
  },
  ativo: {
    type: String
  },
  abl: {
    type: String
  },
  cnpj: {
    type: String
  },
  data: {
    type: Date
  },
  dividend_yeld: {
    type: Number
  },
  gestor: {
    type: String
  },
  inicio: {
    type: String
  },
  media_ano: {
    type: Number
  },
  name: {
    type: String
  },
  preco_inicial: {
    type: String
  },
  preco_mercado: {
    type: Number
  },
  preco_patrimonial: {
    type: Number
  },
  preco_vpa: {
    type: Number
  },
  preco_vpa_desagio: {
    type: String
  },
  quotas: {
    type: Number
  },
  razao_social: {
    type: String
  },
  rendimento_ano: {
    type: Number
  },
  segmento: {
    type: String
  },
  ultima_data_ex: {
    type: String
  },
  ultimo_rendimento: {
    type: Number
  },
  valor_patrimonial: {
    type: Number
  },
  variacao: {
    type: String
  },
  mandato: {
    type: String
  },
  query: [
    {
      pctChange: {
        type: Number
      },
      close: {
        type: Number
      },
      date: {
        type: String
      },
      change: {
        type: Number
      }
    }
  ],
  comunicados: [
    {
      titulo: {
        type: String
      },
      link: {
        type: String
      }
    }
  ],
  ativos_cidade: [
    {
      cidade: {
        type: String
      },
      abl: {
        type: Number
      },
      rep: {
        type: Number
      }
    }
  ],
  ativos_fundo: [
    {
      ativo: {
        type: String
      },
      valor: {
        type: Number
      },
      valor_justo: {
        type: String
      },
      representacao: {
        type: Number
      },
      rep: {
        type: String
      },
      valorizacao: {
        type: String
      }
    }
  ],
  dividendos_list: [
    {
      data_base: {
        type: Date
      },
      data_pagamento: {
        type: Date
      },
      dividendo: {
        type: Number
      }
    }
  ],
  indexadores: [
    {
      indexador: {
        type: String
      },
      premio: {
        type: Number
      },
      quantidade: {
        type: Number
      }
    }
  ],
  papeis_ativos: [
    {
      indexado: {
        type: String
      },
      cri: {
        type: String
      },
      peso: {
        type: Number
      },
      premi: {
        type: Number
      }
    }
  ],
  dados_intrisecos: {
    agio: Number,
    dy_pro: Number,
    updatedAt: Date,
    despe: Number,
    vp: Number,
    ref: String,
    aloc: Number,
    rent: Number
  },
  div_list: {
    dados: [Number],
    messes: [String]
  }
});

module.exports = mongoose.model('Fiis', schema)
