'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  fullname: {
    type: String,
    required: [true, "Onde está seu nome?"]
  },
  nick: {
    type: String,
    required: [true, "Onde está seu apelido?"]
  },
  email: {
    type: String,
    required: [true, "Precisamos do seu email!"],
    trim: true,
    index: true,
    unique: true
  },
  state: {
    type: String,
    required: true
  },
  createdAt: {
    type: String,
    required: true,
    default: new Date()
  },
  passResetExpires: {
    type: Date
  },
  passResetToken: {
    type: String
  },
  admin_token_data: {
    type: String
  },
  accountTokenValidate: {
    type: String
  },
  status: {
    type: String,
    enum: ["active", "deactive"],
    default: "deactive"
  },
  password: {
    type: String,
    required: true
  }
});

module.exports = mongoose.model('Customer', schema)