'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    fundo_invest_qualif: {
        type: Boolean
    },
    cotistas: {
        type: Number
    },
    admin_nome: {
        type: String
    },
    fundo_benchmark: {
        type: String
    },
    patrimonio: {
        type: Number
    },
    gestor_nome: {
        type: String
    },
    fundo_tribut_lp: {
        type: Boolean
    },
    gestor_cnpj: {
        type: Number
    },
    cvm_classe: {
        type: String
    },
    admin_cnpj: {
        type: Number
    },
    fundo_nome_abreviado: {
        type: String
    },
    fundo_cnpj: {
        type: Number
    },
    unique_slug: {
        type: String
    },
    fundo_exclusivo: {
        type: Boolean
    },
    situacao: {
        type: String
    },
    fundo_nome: {
        type: String
    },
    fundo_de_cotas: {
        type: Boolean
    },
    fundo_cond_aberto: {
        type: Boolean
    },
    cotacoes: [{
        q: {
            type: Number
        },
        d: {
            type: String
        },
        p: {
            type: Number
        },
        c: {
            type: Number
        }
    }]
});

module.exports = mongoose.model('Fundos', schema)