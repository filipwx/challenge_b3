'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  wallet: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Wallet'
  },
  ativo: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Fiis'
  },
  createdAt: {
    type: Date,
    required: true,
    default: Date.now
  },
  quantity: {
    type: Number,
    required: true,
    default: 1
  },
  price: {
    type: Number,
    required: true,
    default: 0
  },
  final_total: {
    type: Number,
    required: true,
    default: 0
  },
  final_price: {
    type: Number,
    required: true,
    default: 0
  },
  data: {
    type: Date,
    required: true,
    default: Date.now
  },
  delete: {
    type: Boolean,
    required: true,
    default: false
  },
  close: {
    type: Boolean,
    required: true,
    default: false
  },
  close_date: {
    type: Date
  },
});

module.exports = mongoose.model('Item', schema)
