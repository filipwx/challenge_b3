'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Customer'
  },
  cod: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  investmentProof: {
    type: String
  },
  agreeHash: {
    type: String,
    required: true
  },
  delete: {
    type: Boolean,
    required: true,
    default: false
  },
  locked: {
    type: Boolean,
    required: true,
    default: false
  },
  enabled: {
    type: Boolean,
    required: true,
    default: false
  },
  enabledDate: {
    type: Date
  },
  createdAt: {
    type: String,
    required: true,
    default: new Date()
  },
  total: {
    type: Number,
    required: true,
    default: 100000
  },
  valor_mercado: {
    type: Number,
    required: true,
    default: 0
  },
  historico: [{
    resultado_por_ativo: [{
      resultado: {
        type: Number,
        required: true,
        default: 0
      },
      resultado_p: {
        type: Number,
        required: true,
        default: 0
      },
      valor_pago: {
        type: Number,
        required: true,
        default: 0
      },
      valor_total: {
        type: Number,
        required: true,
        default: 0
      },
      ativo_name: {
        type: String,
        required: true,
        default: ''
      },
      ativo_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Fiis'
      },
    }],
    resultado_final: {
      type: Number,
      required: true,
      default: 0
    },
    valor_pago: {
      type: Number,
      required: true,
      default: 0
    },
    valor_mercado: {
      type: Number,
      required: true,
      default: 0
    },
    variacao: {
      type: Number,
      required: true,
      default: 0
    },
    createdAt: {
      type: Date,
      required: true,
      default: Date.now
    },
  }],
  ativos: [{
    ativo: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Acoes'
    },
    ativo_name: {
      type: String,
      required: true,
      default: ''
    },
    createdAt: {
      type: Date,
      required: true,
      default: Date.now
    },
    quantity: {
      type: Number,
      required: true,
      default: 1
    },
    price: {
      type: Number,
      required: true,
      default: 0
    },
    final_total: {
      type: Number,
      required: true,
      default: 0
    },
    delete: {
      type: Boolean,
      required: true,
      default: false
    },
    close: {
      type: Boolean,
      required: true,
      default: false
    },
    close_date: {
      type: Date
    },
  }],
});

module.exports = mongoose.model('Wallet', schema)
