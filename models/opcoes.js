'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    opcao: {
        type: String
    },
    strike: {
        type: String
    },
    valor: {
        type: String
    },
    vencimento: {
        type: String
    },
    tipo: {
        type: String
    },
    variacao_dia: {
        type: String
    },
    buy: {
        type: String
    },
    sell: {
        type: String
    },
    valor_intrinsico: {
        type: String
    },
    valor_extrinsico: {
        type: String
    },
    negocios: {
        type: String
    },
    data: {
        type: String
    }
});

module.exports = mongoose.model('Opcoes', schema)