'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    user_id: {
        type: String,
        required: true
    },
    data: {
        type: String
    },
    dy_medio: {
        type: Number
    },
    profit_reais: {
        type: Number
    },
    wallet_id: {
        type: String,
        required: true
    },
    total: {
        type: Number
    },
    total_pago: {
        type: Number
    },
    ativos: {
        type: Number
    },
});

module.exports = mongoose.model('historic', schema)