'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  fiis: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Fiis'
  },
  data: {
    type: String
  },
  link: {
    type: String
  },
  titulo: {
    type: String
  }
});

module.exports = mongoose.model('Docs', schema)