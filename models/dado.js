'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  index_ticker: {
    type: String,
    required: true
  },
  index_name: {
    type: String,
    required: true
  },
  index_quotes: [{
    c: {
      type: String,
      required: true
    },
    d: {
      type: String,
      required: true
    }
  }]
});

module.exports = mongoose.model('Dado', schema)
