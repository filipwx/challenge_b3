"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const schema = new Schema({
  login: {
    type: Boolean,
    default: true,
    required: true
  },
  cadastro: {
    type: Boolean,
    default: true,
    required: true
  },
  toolbar: {
    type: Boolean,
    default: true,
    required: true
  }
});

module.exports = mongoose.model("Config", schema);
