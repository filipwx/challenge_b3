'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controlers/item-controler');
const authService = require('../services/auth');

router.post('/', controller.post);
router.post('/liquidate', controller.processSell);

module.exports = router;
