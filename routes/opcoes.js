'use strict'

const express = require('express');
const router = express.Router();
const controler = require('../controlers/opcoes-controler')

router.get('/', controler.get);
router.get('/:id', controler.getById);

module.exports = router;
