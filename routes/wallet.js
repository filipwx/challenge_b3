'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controlers/wallet-controler');
const authService = require('../services/auth');

router.get('/unica/:id', authService.authorize, controller.getById);
router.get('/', authService.authorize, controller.get);
router.get('/proprio', authService.authorize, controller.getWallet);
router.get('/profile-inside', authService.authorize, controller.getAllWallets);
router.get('/single-wallet', authService.authorize, controller.getSingleWallet);
router.get('/list-all-wallets', controller.listAllWallets);
router.post('/', authService.authorize, controller.post);
router.post('/newitem', authService.authorize, controller.setNewItemWallet);
router.get('/listobj', authService.authorize, controller.listObj);
router.get('/delete-wallet/:id', authService.authorize, controller.deleteWallet);
router.get('/situation-wallet/', authService.authorize, controller.getSituationWallet);
router.post('/situation-wallet/', authService.authorize, controller.setSituationWallet);

router.post('/chart-wallet/', authService.authorize, controller.getChartWallet);
router.post('/activate-wallet/', authService.authorize, controller.activateWallet);


module.exports = router;
