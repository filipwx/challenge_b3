'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controlers/master-controler');

router.get('/', controller.get);
router.get('/ranking', controller.getwalletsrank);
router.get('/wallet-iten', controller.walletsItens);
router.get('/situation-wallet/', controller.getSituationWallet);
router.post('/fund-price', controller.queryPrice);

module.exports = router;
