'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controlers/custumer-controler');
const authService = require('../services/auth');

router.post('/', controller.post);
router.post('/authenticate', controller.authenticate);
router.post("/verificar", controller.verificar);
router.post('/refresh-token', authService.authorize, controller.refreshToken);
router.get('/get-user', authService.authorize, controller.getProfile);
router.post('/forgot-pass', controller.fogot);
router.post('/reset-password', controller.redefinir);
router.post('/change-password', authService.authorize, controller.trocarSenha);
router.put('/update', authService.authorize, controller.put);

module.exports = router;
