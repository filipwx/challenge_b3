'use strict'

const express = require('express');
const router = express.Router();
const blockchain = require("../services/blockchain");
const controller = require("../controlers/admin-controler");

router.get('/', async (req, res, next) => {
  res.status(200).send({
    title: "Rodando liso a 1080P",
    version: '1.0.0.9',
    // user: user,
  });
  // try {
  // const user = await blockchain.decodeToken(req.headers)
  // } catch (e) {
  //   res.status(400).send({
  //     message: 'Falha na requisição',
  //     error: e
  //   });
  // }
});
router.get("/configuracoes", controller.getConfig);
module.exports = router;
