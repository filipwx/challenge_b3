'use strict'

const express = require('express');
const router = express.Router();
const controler = require('../controlers/docs-controler')

router.get('/', controler.get);

module.exports = router;
