'use strict'

const express = require('express');
const router = express.Router();
const controler = require('../controlers/fii-controler')

router.get('/', controler.get);
router.get('/full', controler.getAll);
router.get('/:id', controler.getById);
router.get('/delete/:id', controler.deleteFii);
router.put('/master/:id', controler.editFii);

module.exports = router;
