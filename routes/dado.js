'use strict';

const express = require('express');
const router = express.Router();
const controller = require('../controlers/dado-controler');

router.get('/ibov', controller.getIbov);
router.get('/cdi', controller.getCdi);


module.exports = router;
