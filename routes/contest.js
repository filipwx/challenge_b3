'use strict'

const express = require('express');
const router = express.Router();
const controler = require('../controlers/contest-controler')

router.get('/', controler.get);
router.post('/', controler.post);
router.get('/:id', controler.getById);

module.exports = router;
