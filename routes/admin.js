"use strict";

const express = require("express");
const router = express.Router();
const controller = require("../controlers/admin-controler");
const authService = require("../services/auth");

router.get("/wallet", authService.authorize, controller.getAllWallets);
router.get("/users", authService.authorize, controller.getAllUsers);
router.get("/configs", authService.authorize, controller.getAllConfigs);
router.post("/configs/create", authService.authorize, controller.setConfigs);
router.post("/configs/edit", authService.authorize, controller.editConfigs);
router.get("/contests", authService.authorize, controller.getAllContest);
router.post("/contests/create", authService.authorize, controller.createContest);
router.post("/contests/edit/:id", authService.authorize, controller.editContest);
router.post("/autenticate", controller.authenticate);



module.exports = router;