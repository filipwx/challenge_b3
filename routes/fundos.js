'use strict'

const express = require('express');
const router = express.Router();
const controler = require('../controlers/fundos-controler')

router.get('/', controler.get);
router.get('/:id', controler.getById);
router.post('/', controler.post);

module.exports = router;
