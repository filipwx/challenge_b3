const mailer = require('../services/mailer');
  
mailer.sendMail({
  to: req.body.email,
  from: '"Gente Boa" contact@oxigen.club',
  replyTo: 'contact@oxigen.club',
  subject: 'Email de recuperação de senha',
  template: 'forget/forgot',
  context: { token, user }
})